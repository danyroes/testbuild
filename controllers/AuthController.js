const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudare6ed/collections/";
const mLabAPIKey = "apiKey=Ic1v0kegSp7qNlq4tD5poJvRLWRd6MwX";

function logInV1(req,res){
  console.log("POST /apitechu/v1/login");
  //console.log(req.headers);
  console.log("Email is: " + req.body.email);
  console.log("Password is: " + req.body.password);

  var users = require('../usuarios.json');

  var userId = '';
  for (user of users) {
       console.log("User is " + user.first_name);
       console.log("email is " + user.email);
       console.log("password is " + user.password);

       if (user.email == req.body.email && user.password == req.body.password) {
         console.log("El email y password coinciden");
         user.logged = true;
         userId = user.id;
         io.writeUserDataToFile(users);
         break;
       }
    }

  if (userId != ''){
    var logInUser = {
      "mensaje" : "Login correcto",
      "idUsuario" : userId
    };

  }else{
    var logInUser = {
      "mensaje" : "Login incorrecto"
    };
  }


  console.log("Usuario logado con éxito");

  res.send(logInUser);

  /*
  Aquí poner el código del profe

  */
}

function logInV2(req,res){
  console.log("POST /apitechu/v2/login");
  console.log("Email is: " + req.body.email);
  console.log("Password is: " + req.body.password);

  var email = req.body.email;
  var password = req.body.password;

  if (!email || !password) {
     res.send(
       {
         "mensaje" : "Login incorrecto, necesarios email y password."
       }
     )
  };

  var query = 'q={"email":"' + email +'"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log('Consultando cliente');
  console.log("httpClient.get=user?"+ query + "&" + mLabAPIKey);
  httpClient.get('user?'+ query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      console.log("Hemos entrado");
      console.log(body);
      var isPasswordcorrect = crypt.checkPassword(password,body[0].password);
      if (body.length == 0 || !isPasswordcorrect) {
        var response = {
          "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
          }
        res.send(response);
      }else{
        console.log("Got a user with that email and password, logging in");
        query = 'q={"id" : ' + body[0].id +'}';
        console.log("Query for put is " + query);
        var putBody = '{"$set":{"logged":true}}';
        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            console.log("PUT done");
            var response = {
              "msg" : "Usuario logado con éxito",
              "idUsuario" : body[0].id
            }
            res.send(response);
          }
        );
      }
    }
  );

}

function logOutV1(req,res){
  console.log("POST /apitechu/v1/logout");
  //console.log(req.headers);
  console.log("Email is: " + req.body.email);
  console.log("Password is: " + req.body.password);

  var users = require('../usuarios.json');

  var userId = '';
  for (user of users) {
      console.log("User is " + user.first_name);
      console.log("email is " + user.email);
      console.log("password is " + user.password);

      if (user.email == req.body.email && user.password == req.body.password) {
          console.log("El email y password coinciden");
          delete user.logged;
          userId = user.id;
          io.writeUserDataToFile(users);
          break;
      }
  }

       if (userId != ''){
         var logInUser = {
           "mensaje" : "Logout correcto",
           "idUsuario" : userId
         };
         console.log("Usuario deslogado con éxito");
       }else{
         var logInUser = {
           "mensaje" : "Logout incorrecto"
         };
         console.log("Usuario deslogado sin éxito");
       }


  res.send(logInUser);
}

function logOutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}


module.exports.logInV1 = logInV1;
module.exports.logInV2 = logInV2;
module.exports.logOutV1 = logOutV1;
module.exports.logOutV2 = logOutV2;
